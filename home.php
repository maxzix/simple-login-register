<?php

	require_once("session.php");
	
	require_once("class.user.php");
	$auth_user = new USER();
	
	
	$user_id = $_SESSION['user_session'];
	
	$stmt = $auth_user->runQuery("SELECT * FROM users WHERE id=:user_id");
	$stmt->execute(array(":user_id"=>$user_id));
	
	$userRow=$stmt->fetch(PDO::FETCH_ASSOC);

?>
<!DOCTYPE html>
   <html>
      <head>
         <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
         <link rel="stylesheet" href="style.css" type="text/css"/>
         <title>Welcome - <?php print($userRow['username']); ?></title>
      </head>
      <body>
         <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
               <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                     <span class="sr-only">Toggle navigation</span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                  </button>
               </div>
               <div id="navbar" class="navbar-collapse collapse">
                  <ul class="nav navbar-nav">
                  </ul>
                  <ul class="nav navbar-nav navbar-right">
                     <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
   			               <span class="glyphicon glyphicon-user"></span>&nbsp;Hi' <?php echo $userRow['username']; ?>&nbsp;<span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                           <li><a href="logout.php?logout=true"><span class="glyphicon glyphicon-log-out"></span>&nbsp;Sign Out</a></li>
                        </ul>
                     </li>
                  </ul>
               </div>
            </div>
         </nav> 
         <div class="clearfix"></div>
   
         <div class="container-fluid" style="margin-top:80px;">
            <div class="container">
               <div class="row">
                  <div class="col-xs-12 text-center">
                     <h2>Information</h2>
                  </div>
                  <div class="col-md-6 col-md-offset-3">
                      <ul class="list-group">
                        <li class="list-group-item text-right">
                           <span class="pull-left"><strong class="">Name</strong></span> <?php echo $userRow['first_name']; ?>
                        </li>
                        <li class="list-group-item text-right">
                           <span class="pull-left"><strong class="">Surname</strong></span> <?php echo $userRow['last_name']; ?>
                        </li>
                        <li class="list-group-item text-right">
                           <span class="pull-left"><strong class="">Username</strong></span> <?php echo $userRow['username']; ?>
                        </li>
                        <li class="list-group-item text-right">
                           <span class="pull-left"><strong class="">Birth</strong></span> <?php echo $userRow['birth']; ?>
                        </li>
                        <li class="list-group-item text-right">
                           <span class="pull-left"><strong class="">Gender</strong></span> <?php echo $userRow['gender']; ?>
                        </li>
                        <li class="list-group-item text-right">
                           <span class="pull-left"><strong class="">Country</strong></span> <?php echo $userRow['country']; ?>
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</body>
</html>