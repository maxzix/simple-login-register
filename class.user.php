<?php

require_once('dbconfig.php');

class USER
{	

	private $conn;
	
	public function __construct()
	{
		$database = new Database();
		$db = $database->dbConnection();
		$this->conn = $db;
    }
	
	public function runQuery($sql)
	{
		$stmt = $this->conn->prepare($sql);
		return $stmt;
	}
	
	public function register($uUsername,$uEmail,$uPassword,$uFirstName,$uLastName,$uBirth,$uGender,$uCountry)
	{
		try
		{
			$new_password = password_hash($uPassword, PASSWORD_DEFAULT);
			
			$stmt = $this->conn->prepare("INSERT INTO users(first_name,last_name,email,username,password,birth,gender,country) 
		                                               VALUES(:uFirstName, :uLastName, :uEmail, :uUsername, :uPassword, :uBirth, :uGender, :uCountry)");
												  
			$stmt->bindparam(":uFirstName", $uFirstName);
			$stmt->bindparam(":uLastName", $uLastName);
			$stmt->bindparam(":uEmail", $uEmail);
			$stmt->bindparam(":uUsername", $uUsername);
			$stmt->bindparam(":uPassword", $new_password);
			$stmt->bindparam(":uBirth", $uBirth);
			$stmt->bindparam(":uGender", $uGender);
			$stmt->bindparam(":uCountry", $uCountry);										  
				
			$stmt->execute();	
			
			return $stmt;	
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}				
	}
	
	
	public function doLogin($uUsername,$uEmail,$uPassword)
	{
		try
		{
			$stmt = $this->conn->prepare("SELECT id, username, email, password FROM users WHERE username=:uUsername OR email=:uEmail ");
			$stmt->execute(array(':uUsername'=>$uUsername, ':uEmail'=>$uEmail));
			$userRow=$stmt->fetch(PDO::FETCH_ASSOC);
			if($stmt->rowCount() == 1)
			{
				if(password_verify($uPassword, $userRow['password']))
				{
					$_SESSION['user_session'] = $userRow['id'];
					return true;
				}
				else
				{
					return false;
				}
			}
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}
	
	public function is_loggedin()
	{
		if(isset($_SESSION['user_session']))
		{
			return true;
		}
	}
	
	public function redirect($url)
	{
		header("Location: $url");
	}
	
	public function doLogout()
	{
		session_destroy();
		unset($_SESSION['user_session']);
		return true;
	}
}
?>