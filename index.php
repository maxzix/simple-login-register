<?php
session_start();
require_once("class.user.php");
$login = new USER();

if($login->is_loggedin()!="")
{
	$login->redirect('home.php');
}

if(isset($_POST['btn-login']))
{
	$uname = strip_tags($_POST['txt_uname_email']);
	$umail = strip_tags($_POST['txt_uname_email']);
	$upass = strip_tags($_POST['txt_password']);
		
	if($login->doLogin($uname,$umail,$upass))
	{
		$login->redirect('home.php');
	}
	else
	{
		$error = "Wrong Details !";
	}
}
?>
<!DOCTYPE html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Login</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="style.css" type="text/css"/>
    </head>
    
    <body>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <form class="form-signin" method="post" id="login-form">
                        <h2 class="text-center">Log In</h2><hr />
                        <div id="error">
                            <?php
                                if(isset($error))
                                {
                                    ?>
                                    <div class="alert alert-danger">
                                       <i class="glyphicon glyphicon-warning-sign"></i> &nbsp; <?php echo $error; ?> !
                                    </div>
                                    <?php
                                }
                            ?>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="txt_uname_email" placeholder="Username or E mail ID" required />
                            <span id="check-e"></span>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" name="txt_password" placeholder="Your Password" />
                        </div>
                        <hr />
                        <div class="form-group text-center">
                            <button type="submit" name="btn-login" class="btn btn-default">
                                <i class="glyphicon glyphicon-log-in"></i> &nbsp; SIGN IN
                            </button>
                        </div>  
                        <br />
                        <label>Don't have account yet ! <a href="sign-up.php">Sign Up</a></label>
                    </form>
                </div>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="assets/js/custom.js"></script>
    </body>
</html>