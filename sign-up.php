<?php
session_start();
require_once('class.user.php');
$user = new USER();

if($user->is_loggedin()!="")
{
	$user->redirect('home.php');
}

if(isset($_POST['btn-signup']))
{
	$uUsername = strip_tags($_POST['txtUsername']);
	$uEmail = strip_tags($_POST['txtEmail']);
	$uPassword = strip_tags($_POST['txtPass']);
	$uFirstName = strip_tags($_POST['txtFirstName']);	
	$uLastName = strip_tags($_POST['txtLastName']);	
	$uBirth = strip_tags($_POST['txtBirth']);	
	$uCountry = strip_tags($_POST['txtCountry']);		
	$uGender = strip_tags($_POST['txtGender']);	

	if($uUsername=="")	{
		$error[] = "provide username !";	
	}
	else if($uEmail=="")	{
		$error[] = "provide email id !";	
	}
	else if(!filter_var($uEmail, FILTER_VALIDATE_EMAIL))	{
	    $error[] = 'Please enter a valid email address !';
	}
	else if($uPassword=="")	{
		$error[] = "provide password !";
	}
	else if(strlen($uPassword) < 6){
		$error[] = "Password must be atleast 6 characters !";	
	}
	else if($uFirstName=="")	{
		$error[] = "provide your first name !";	
	}
	else if($uLastName=="")	{
		$error[] = "provide your last name !";	
	}
	else if($uBirth=="")	{
		$error[] = "provide date of birth !";	
	}
	else if($uGender=="")	{
		$error[] = "provide gender !";	
	}
	else if($uCountry=="")	{
		$error[] = "provide your country !";	
	}
	else
	{
		try
		{
			$stmt = $user->runQuery("SELECT username, email FROM users WHERE username=:uUsername OR email=:uEmail");
			$stmt->execute(array(':uUsername'=>$uUsername, ':uEmail'=>$uEmail));
			$row=$stmt->fetch(PDO::FETCH_ASSOC);
				
			if($row['username']==$uUsername) {
				$error[] = "sorry username already taken !";
			}
			else if($row['email']==$uEmail) {
				$error[] = "sorry email id already taken !";
			}
			else
			{
				if($user->register($uUsername,$uEmail,$uPassword,$uFirstName,$uLastName,$uBirth,$uGender,$uCountry)){	
					$user->redirect('sign-up.php?joined');
				}
			}
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}	
}

?>

<!DOCTYPE html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Register</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="style.css" type="text/css"/>
    </head>
    
    <body>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <form class="form-signin" method="post" id="login-form">
                        <h2 class="text-center">Register</h2><hr />
                        <?php
							if(isset($error)){
							 	foreach($error as $error){
						?>
				                    <div class="alert alert-danger">
				                        <i class="glyphicon glyphicon-warning-sign"></i> &nbsp; <?php echo $error; ?>
				                    </div>
				        <?php
								}
							}else if(isset($_GET['joined'])){
						?>
				                <div class="alert alert-info">
				                    <i class="glyphicon glyphicon-log-in"></i> &nbsp; Successfully registered <a href='index.php'>login</a> here
				                </div>
				        <?php
							}
						?>
						<div class="form-group">
                            <input type="text" class="form-control" name="txtFirstName" placeholder="First name" value="<?php if(isset($error)){echo $uFirstName;}?>" />
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="txtLastName" placeholder="Last name" value="<?php if(isset($error)){echo $uLastName;}?>" />
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="txtEmail" placeholder="E-Mail" value="<?php if(isset($error)){echo $uEmail;}?>" />
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="txtUsername" placeholder="Username" value="<?php if(isset($error)){echo $uUsername;}?>" />
                        </div>
                        <div class="form-group">
            				<input type="password" class="form-control" name="txtPass" placeholder="Enter Password" />
            			</div>
            			<div class="form-group">
                            <input type="date" class="form-control" name="txtBirth" placeholder="Date of birth" value="<?php if(isset($error)){echo $uBirth;}?>" />
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="txtCountry" placeholder="Country" value="<?php if(isset($error)){echo $uCountry;}?>" />
                        </div>
                        <div class="form-group">
                        	<label class="radio-inline"><input type="radio" name="txtGender" value="male">Male</label>
							<label class="radio-inline"><input type="radio" name="txtGender" value="female">Female</label>
                        </div>
                        <hr />
                        <div class="form-group text-center">
            				<button type="submit" class="btn btn-primary" name="btn-signup">
                				<i class="glyphicon glyphicon-open-file"></i>&nbsp;SIGN UP
                			</button>
            			</div> 
                        <br />
                        <label>have an account ! <a href="index.php">Sign In</a></label>
                    </form>
                </div>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="assets/js/custom.js"></script>
    </body>
</html>